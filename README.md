#Accessible Navigation for Disability Hub MN

This is an incomplete build with source code only. The purpose of this repo and code is to show style and intent. The relevant HTML, CSS, and Javascript View file are found herein. The stylesheets are post-processed CSS utilizing [PostCSS](https://github.com/postcss/postcss) and a few plugins specific to the project for efficiency.
The code cannot be built and is view only.

Some things are assumed such as script references, stylesheet references, build references, asset imports and the like are at a more global level, not included herein.